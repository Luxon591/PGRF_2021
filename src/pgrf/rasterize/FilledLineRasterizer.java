package pgrf.rasterize;

public class FilledLineRasterizer extends LineRasterizer {
    public FilledLineRasterizer(Raster raster){
        super(raster);
    }

    /*
        Triviální algoritmus

        - neefektivní práce s desetinou čárkou

        + jednoduchý a dobře použitelný i na složitější křivky

     */

    @Override
    public void rasterize(int x1, int y1, int x2, int y2, int color) {
        float k = (y2 - y1) / (float) (x2 - x1);

        float q = y1 - k * x1;

        for (int x = x1; x <= x2; x++) {
            float y = k * x + q;

            raster.setPixel(x, Math.round(y), color);
        }

        for (int x = x2; x <= x1; x++) {
            float y = k * x + q;

            raster.setPixel(x, Math.round(y), color);
        }

        for (int y = y1; y <= y2; y++) {
            float x = (y - q) / k;

            raster.setPixel(Math.round(x), y, color);
        }

        for (int y = y2; y <= y1; y++) {
            float x = (y - q) / k;

            raster.setPixel(Math.round(x), y, color);
        }
    }
}
