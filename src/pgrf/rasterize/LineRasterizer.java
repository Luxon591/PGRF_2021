package pgrf.rasterize;

import java.awt.*;

public abstract class LineRasterizer {
    protected Raster raster;

    public LineRasterizer(Raster raster){
        this.raster = raster;
    }

    public void rasterize(Point p1, Point p2, int color){
        rasterize(p1.x, p1.y, p2.x, p2.y, color);
    }

    public void rasterize(Point p1, Point p2){
        rasterize(p1.x, p1.y, p2.x, p2.y);
    }

    void rasterize(int x1, int y1, int x2, int y2){
        rasterize(x1, y1, x2, y2, raster.getForegroundColor());
    }

    public abstract void rasterize(int x1, int y1, int x2, int y2, int color);
}