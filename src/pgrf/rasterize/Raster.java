package pgrf.rasterize;

import java.awt.*;
import java.awt.image.BufferedImage;

public interface Raster {
    int getWidth();
    int getHeight();
    void setBackground(int color);
    int getBackground();
    void clear();
    int getPixel(int x, int y);
    void setPixel(Point point);
    void setPixel(int x, int y, int color);
    void setPixel(int x, int y);
    void setForegroundColor(int color);
    int getForegroundColor();
}