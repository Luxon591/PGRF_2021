package pgrf.rasterize;

import java.awt.*;
import java.awt.image.BufferedImage;

public class RasterBufferedImage implements Raster
{
    private int foregroundColor = Color.yellow.getRGB();
    private int backgroundColor = 0x0;

    private BufferedImage img;

    public RasterBufferedImage(BufferedImage img){
        this.img = img;
    }

    public BufferedImage GetBufferedImage(){
        return this.img;
    }

    @Override
    public int getWidth() {
        return img.getWidth();
    }

    @Override
    public int getHeight() {
        return img.getHeight();
    }

    @Override
    public void setBackground(int color) {
        this.backgroundColor = color;
    }

    @Override
    public int getBackground() {
        return backgroundColor;
    }

    @Override
    public void clear() {
        Graphics gr = img.getGraphics();
        gr.setColor(new Color(backgroundColor));
        gr.fillRect(0, 0, img.getWidth(), img.getHeight());
    }

    public void draw(RasterBufferedImage raster) {
        Graphics g = img.getGraphics();
        g.setColor(new Color(backgroundColor));
        g.fillRect(0, 0, getWidth(), getHeight());
        g.drawImage(raster.img, 0, 0, null);
    }

    @Override
    public int getPixel(int x, int y) {
        return img.getRGB(x, y);
    }

    @Override
    public void setPixel(Point point){
        setPixel(point.x, point.y, this.getForegroundColor());
    }

    @Override
    public void setPixel(int x, int y, int color) {
        if (x > 0 && y > 0 && x < img.getWidth() && y < img.getHeight()){
            img.setRGB(x, y, color);
        }
    }

    @Override
    public void setPixel(int x, int y) {
        setPixel(x, y, foregroundColor);
    }

    @Override
    public void setForegroundColor(int color) {
        this.foregroundColor = color;
    }

    @Override
    public int getForegroundColor() {
        return this.foregroundColor;
    }
}
