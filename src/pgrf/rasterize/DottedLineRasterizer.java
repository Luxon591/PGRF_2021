package pgrf.rasterize;

import java.awt.*;
import java.util.ArrayList;

public class DottedLineRasterizer extends LineRasterizer {
    private int spaceSize;

    public DottedLineRasterizer(Raster raster){
        super(raster);

        spaceSize = 4;
    }

    @Override
    public void rasterize(int x1, int y1, int x2, int y2, int color) {
        ArrayList<Point> allLinePoints = new ArrayList<Point>();
        ArrayList<Point> partialPoints = new ArrayList<Point>();

        float k = (y2 - y1) / (float) (x2 - x1);

        float q = y1 - k * x1;

        for (int x = x1; x <= x2; x++) {
            float y = k * x + q;

            allLinePoints.add(new Point(x, Math.round(y)));
        }

        for (int x = x2; x <= x1; x++) {
            float y = k * x + q;

            allLinePoints.add(new Point(x, Math.round(y)));
        }

        for (int y = y1; y <= y2; y++) {
            float x = (y - q) / k;

            allLinePoints.add(new Point(Math.round(x), y));
        }

        for (int y = y2; y <= y1; y++) {
            float x = (y - q) / k;

            allLinePoints.add(new Point(Math.round(x), y));
        }

        for (int i = 0; i <= allLinePoints.size() / 2; i = i + spaceSize)
        {
            int reversedIndex = (allLinePoints.size() - 1) - i;

            partialPoints.add(allLinePoints.get(reversedIndex));
            partialPoints.add(allLinePoints.get(i));
        }

        for (Point p : partialPoints){
            raster.setPixel(p);
        }
    }
}
