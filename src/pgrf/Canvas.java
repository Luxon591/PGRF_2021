package pgrf;

import pgrf.model.Polygon;
import pgrf.model.Triangle;
import pgrf.rasterize.*;

import java.awt.*;
import java.awt.event.*;
import java.awt.image.BufferedImage;

import javax.swing.*;

/**
 * trida pro kresleni na platno: zobrazeni pixelu
 *
 * @author PGRF FIM UHK
 * @version 2020
 */

public class Canvas {

	private JFrame frame;
	private JPanel panel;
	private Raster raster;
	private LineRasterizer lineRasterizer;

	private LineRasterizer dottedLineRasterizer;
	private LineRasterizer solidLineRasterizer;

	private boolean triangleMode = false;
	private boolean editMode = false;

	private Point nearestPoint = null;

	private Polygon polygon;
	private Triangle triangle;

	public Canvas(int width, int height) {
		frame = new JFrame();

		frame.setLayout(new BorderLayout());
		frame.setTitle("UHK FIM PGRF : " + this.getClass().getName());
		frame.setResizable(false);
		frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);

		raster = new RasterBufferedImage(new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB));

		solidLineRasterizer = new FilledLineRasterizer(raster);
		dottedLineRasterizer = new DottedLineRasterizer(raster);

		//Preset default rasterizer
		lineRasterizer = solidLineRasterizer;

		panel = new JPanel() {
			private static final long serialVersionUID = 1L;

			@Override
			public void paintComponent(Graphics g) {
				super.paintComponent(g);
				present(g);
			}
		};

		panel.setPreferredSize(new Dimension(width, height));

		frame.add(panel, BorderLayout.CENTER);
		frame.pack();
		frame.setVisible(true);

		initListeners();

		polygon = new Polygon();
		polygon.setColor(raster.getForegroundColor());

		triangle = new Triangle();
	}

	public void clearAll() {
		raster.clear();

		polygon.clear();

		triangle.clear();

		panel.repaint();
	}

	public void update(){
		raster.clear();

		if (triangleMode)
		{
			triangle.rasterizeTriangle(lineRasterizer);
		}
		else
		{
			polygon.rasterize(lineRasterizer);
		}

		panel.repaint();
	}

	public void present(Graphics graphics) {
		graphics.drawImage(((RasterBufferedImage)raster).GetBufferedImage(), 0, 0, null);
	}

	private void initListeners()
	{
		this.frame.addKeyListener(new KeyAdapter()
		{
			@Override
			public void keyPressed(KeyEvent e)
			{
				if (e.getKeyCode() == KeyEvent.VK_C)
				{
					clearAll();
				}

				if (e.getKeyCode() == KeyEvent.VK_T)
				{
					triangleMode = !triangleMode;

					System.out.println("Triangle mode: " + (triangleMode ? "Activated" : "Deactivated"));
				}

				if (e.getKeyCode() == KeyEvent.VK_E)
				{
					editMode = !editMode;

					lineRasterizer = editMode ? dottedLineRasterizer : solidLineRasterizer;

					System.out.println("Edit mode mode: " + (editMode ? "Activated" : "Deactivated"));

					update();
				}
			}
		});


		this.panel.addMouseListener(new MouseAdapter() {
			@Override
			public void mousePressed(MouseEvent e) {

				if (!triangleMode)
				{
					if (editMode)
					{
						nearestPoint = polygon.getNearestPoint(e.getPoint());
					}
					else
					{
						if (polygon.getVertexCount() == 0)
						{
							polygon.addVertex(e.getPoint());
						}
					}
				}
				else
				{
					triangle.addVertex(e.getPoint());
				}

				update();
			}

			@Override
			public void mouseReleased(MouseEvent e) {
				if (!triangleMode)
				{
					if (editMode && nearestPoint != null)
					{
						nearestPoint.setLocation(e.getPoint());
					}
					else
					{
						polygon.addVertex(e.getPoint());
					}

					update();
				}
			}
		});

		this.panel.addMouseMotionListener(
				new MouseMotionAdapter() {

					@Override
					public void mouseDragged(MouseEvent e) {
					    if (editMode && nearestPoint != null)
					    {
                            update();

                            dottedLineRasterizer.rasterize(nearestPoint, e.getPoint(), Color.ORANGE.getRGB());
                        }
					    else
						{
                            if (polygon.getVertexCount() > 0)
                            {
                                update();

                                dottedLineRasterizer.rasterize(polygon.getLastVertex(), e.getPoint());
                                dottedLineRasterizer.rasterize(polygon.getVertices().get(0), e.getPoint());
                            }
                        }
					}
				});
	}

	public static void main(String[] args) {
		SwingUtilities.invokeLater(() -> new Canvas(800, 600));
	}
}
