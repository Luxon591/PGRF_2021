package pgrf.model;

import pgrf.rasterize.LineRasterizer;
import pgrf.utils.Calculator;

import java.awt.*;
import java.util.ArrayList;

public class Polygon {
    private ArrayList<Point> vertices;
    private int color;

    public Polygon() {
        vertices = new ArrayList<Point>();
        color = 0xFF00FF;
    }

    public Polygon(ArrayList<Point> vertices, int color) {
        this.vertices = vertices;
        this.color = color;
    }

    public void addVertex(Point vertex) {
        this.vertices.add(vertex);
    }

    public ArrayList<Point> getVertices() {
        return vertices;
    }

    public Point getLastVertex() {
        if (!vertices.isEmpty())
        {
            return vertices.get(vertices.size() - 1);
        }
        return null;
    }

    public int getVertexCount(){
        return vertices.size();
    }

    public void setVertices(ArrayList<Point> vertices) {
        this.vertices = vertices;
    }

    public int getColor() {
        return color;
    }

    public void setColor(int color) {
        this.color = color;
    }

    public void clear(){
        this.vertices.clear();
    }

    public void rasterize(LineRasterizer rasterizer){
        if (vertices.size() > 2){
            rasterizer.rasterize(vertices.get(0) ,getLastVertex());
        }

        int index = 0;
        while((vertices.size() - (index + 2)) >= 0){

            rasterizer.rasterize(vertices.get(index), vertices.get(index + 1));

            index++;
        }
    }

    public Point getNearestPoint(Point p) {
        Point currentNearest = null;

        if (!vertices.isEmpty()) {
            Point firstPoint = vertices.get(0);
            currentNearest = vertices.get(0);

            int dis = Calculator.GetDistance(firstPoint, p);

            for (int i = 1; i < vertices.size(); i++) {

                Point point = vertices.get(i);

                int distOfCurrentPoint = Calculator.GetDistance(point, p);

                if (distOfCurrentPoint < dis) {
                    currentNearest = point;
                }
            }
        }

        return currentNearest;
    }
}
