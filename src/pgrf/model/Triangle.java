package pgrf.model;

import pgrf.rasterize.LineRasterizer;
import pgrf.rasterize.Raster;
import pgrf.utils.Calculator;

import java.awt.*;
import java.util.ArrayList;
import java.util.List;

public class Triangle {
    private ArrayList<Point> vertices;

    private Point middle;
    private int halfDistance;
    private int wholeDistance;

    public Triangle() {
        vertices = new ArrayList<Point>();
    }

    public Triangle(List<Point> vertices) {
        vertices = new ArrayList<Point>(vertices);
    }

    public void rasterizeTriangle(LineRasterizer rasterizer) {
        if ( vertices.size() == 1 ){
            Point point = vertices.get(0);

            rasterizer.rasterize(point, point, 0x0000FF);
        }

        if ( vertices.size() == 2 ){
            Point firstPoint = vertices.get(0);
            Point secondPoint = vertices.get(1);

            rasterizer.rasterize(firstPoint, secondPoint, 0x0000FF);

            int midX = (firstPoint.x + secondPoint.x)/2;
            int midY = (firstPoint.y + secondPoint.y)/2;

            middle = new Point(midX, midY);

            addVertex(middle);

            halfDistance = Calculator.GetDistance(middle, secondPoint);
        }

        if (vertices.size() == 4){

            Point p0 = vertices.get(0);
            Point p1 = vertices.get(1);
            Point p2 = vertices.get(2);
            Point p3 = vertices.get(3);

            wholeDistance = Calculator.GetDistance(p2, p3);

            int newX = p2.x - (halfDistance *(p2.x - p3.x))/wholeDistance;
            int newY = p2.y - (halfDistance *(p2.y - p3.y))/wholeDistance;

            Point p4 = new Point(newX,newY);

            addVertex(p4);

            rasterizer.rasterize(p0, p1);
            rasterizer.rasterize(p0, p4);
            rasterizer.rasterize(p1, p4);

            rasterizer.rasterize(p2, p3, Color.green.getRGB());
        }

        System.out.println(vertices.size());
    }

    public void addVertex(Point p){
        if (vertices.size() < 5)
        {
            vertices.add(p);
        }
        else
        {
            System.out.println("Triangle cannot add next vertex! Max number of vertices has been reached");
        }
    }

    public void clear(){
        vertices.clear();
    }
}