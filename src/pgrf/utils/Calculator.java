package pgrf.utils;

import java.awt.*;
import java.lang.Math;

public class Calculator {
    public static int GetDistance(Point firstPoint, Point secondPoint) {
        return (int) Math.sqrt(Math.pow(firstPoint.x - secondPoint.getX(), 2) + Math.pow(firstPoint.y - secondPoint.getY(), 2));
    }
}
