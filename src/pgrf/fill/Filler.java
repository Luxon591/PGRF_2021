package pgrf.fill;

import pgrf.rasterize.Raster;

public abstract class Filler {
    protected int fillColor;
    final protected Raster raster;

    public Filler(Raster raster){
        this.raster = raster;
    }

    public void setFiller(int color){
        fillColor = color;
    }

    public abstract void fill();
}
