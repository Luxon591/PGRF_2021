package pgrf.fill;

import pgrf.rasterize.Raster;

public class SeedFiller extends Filler {

    private int seedX;
    private int seedY;

    public SeedFiller(Raster raster){
        super(raster);
    }

    @Override
    public void fill() {

    }

    private void seedFill(int x, int y){
        if (isInside(x, y)){
            raster.setPixel(x, y, fillColor);
            seedFill(x + 1, y);
            seedFill(x - 1, y);
            seedFill(x, y + 1);
            seedFill(x, y - 1);
        }
    }

    private boolean isInside(int x, int y){
        return true;
    }

    public int getSeedX() {
        return seedX;
    }

    public int getSeedY() {
        return seedY;
    }

    public void setSeedX(int seedX) {
        this.seedX = seedX;
    }

    public void setSeedY(int seedY) {
        this.seedY = seedY;
    }
}
